import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  },
  {
    path: 'dashboard',
    pathMatch: 'full',
    loadChildren: () => {
      return import('./dashboard/dashboard.module').then(mod => mod.DashboardModule);
    },
  },
  {
    path: 'categoria',
    pathMatch: 'prefix',
    loadChildren: () => {
      return import('./categoria/categoria.module').then(mod => mod.CategoriaModule);
    },
  },
  {
    path: 'lancamento',
    pathMatch: 'prefix',
    loadChildren: () => {
      return import('./lancamento/lancamento.module').then(mod => mod.LancamentoModule);
    },
  },
  {
    path: 'extrato',
    pathMatch: 'prefix',
    loadChildren: () => {
      return import('./extrato/extrato.module').then(mod => mod.ExtratoModule);
    },
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
