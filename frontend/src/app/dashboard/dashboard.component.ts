import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ItemResumo } from '../core/model/item-resumo';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  view: [number,number] = [900, 900];
  dadosGraficoSaida: any[];
  dadosGraficoEntrada: any[];
  // options
  gradient: boolean = true;
  showLegend: boolean = false;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition = 'below';

  colorScheme = 'vivid';


  constructor(private httpClient: HttpClient) {
    this.dadosGraficoSaida =  [];
    this.dadosGraficoEntrada =  [];
  }

  ngOnInit(): void {
    this.httpClient.get<any>(environment.apiUrl+'/resumo').toPromise().then((result) => {

      result.itensResumo.map((item: ItemResumo) => {
        if (item.tipoCategoria === "SAIDA") {
          this.dadosGraficoSaida.push({
            "name": item.nomeCategoria,
            "value": item.valorTotalCategoria,
          });
        }
        else {
          this.dadosGraficoEntrada.push({
            "name": item.nomeCategoria,
            "value": item.valorTotalCategoria,
          });
        }
      })
      this.dadosGraficoSaida = [...this.dadosGraficoSaida]
      this.dadosGraficoEntrada = [...this.dadosGraficoEntrada]
    })
  }


  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

}
