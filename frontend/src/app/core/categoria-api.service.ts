import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Categoria } from './model/categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriaApiService {

  constructor(private httpClient: HttpClient) { }

  listAll = (): Promise<Categoria[]> => {

    return this.httpClient.get<Categoria[]>(environment.apiUrl + '/categoria').toPromise();
  }

  addCategoria = (nome: string, tipo: string, descricao: string) => {
    return this.httpClient.post<Categoria>(
      environment.apiUrl + '/categoria',
      {
        nome: nome,
        tipo: tipo,
        descricao: descricao
      } as Categoria).toPromise();

  }
}
