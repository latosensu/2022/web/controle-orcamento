import { Lancamento } from './../core/model/lancamento';
import { LancamentoApiService } from './../core/lancamento-api.service';
import { CategoriaApiService } from './../core/categoria-api.service';
import { Categoria } from './../core/model/categoria';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lancamento',
  templateUrl: './lancamento.component.html',
  styleUrls: ['./lancamento.component.scss']
})
export class LancamentoComponent implements OnInit {

  fazerLancamentoForm: FormGroup = this.fb.group({
    valor: [null, [Validators.required, Validators.min(0)]],
    descricao: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(256)]],
    categoria: [null, [Validators.required]]
  });

  categoriaSelecionada: Categoria = {} as Categoria;

  categorias: Categoria[] = [];

  constructor(
    private fb: FormBuilder,
     private router: Router,
     private categoriaApiService: CategoriaApiService,
     private lancamentoApiService: LancamentoApiService
  ) { }

  ngOnInit(): void {
    this.categoriaApiService.listAll().then((categorias) => {
      this.categorias = categorias;
    })
  }

  fazerLancamento = () => {
    const descricao = this.fazerLancamentoForm.get('descricao')?.value;
    const categoria = this.fazerLancamentoForm.get('categoria')?.value;
    const valor = this.fazerLancamentoForm.get('valor')?.value;
    this.lancamentoApiService.adicionarLancamento(descricao,this.categoriaSelecionada, valor).then((lancamento: Lancamento) => {
      alert(`Obrigado. O lançamento ${lancamento.descricao} foi adicionado com sucesso!`);
      this.router.navigateByUrl('/');
    }).catch((error) => {
      alert(`Não foi possível adicionar o lançamento por causa do erro:  ${error.message}!`);
    });
  }

  voltarParaDashboard = () => {
    this.router.navigateByUrl('/')
  }

  onSubmit = () => {
    this.fazerLancamento();
  }

}
