import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UiModule } from '../ui/ui.module';
import { LancamentoRoutingModule } from './lancamento-routing.module';
import { LancamentoComponent } from './lancamento.component';

@NgModule({
  declarations: [
    LancamentoComponent
  ],
  imports: [
    CommonModule,
    LancamentoRoutingModule,
    UiModule
  ]
})
export class LancamentoModule { }
