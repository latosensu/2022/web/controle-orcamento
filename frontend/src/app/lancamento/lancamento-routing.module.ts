import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LancamentoComponent } from './lancamento.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: 'fazer-lancamento'
  },
  {
    path: 'fazer-lancamento',
    pathMatch: 'full',
    component: LancamentoComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
  ]
})
export class LancamentoRoutingModule { }
