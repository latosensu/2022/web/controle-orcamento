import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UiModule } from '../ui/ui.module';
import { CadastroCategoriaComponent } from './cadastro-categoria/cadastro-categoria.component';
import { CategoriaRoutingModule } from './categoria-routing.module';
import { CategoriaComponent } from './categoria.component';

@NgModule({
  declarations: [
    CategoriaComponent,
    CadastroCategoriaComponent
  ],
  imports: [
    CommonModule,
    CategoriaRoutingModule,
    UiModule
  ]
})
export class CategoriaModule { }
