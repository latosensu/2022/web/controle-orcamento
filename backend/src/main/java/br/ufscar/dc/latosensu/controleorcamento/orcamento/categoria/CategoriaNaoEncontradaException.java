package br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria;

/**
 * Exceção lançada quando não existe uma categoria com determinado id.
 */
public class CategoriaNaoEncontradaException extends RuntimeException{
    
    public CategoriaNaoEncontradaException(Long id) {
        super("Não foi possível encontrar a categoria " + id);
    }

}
