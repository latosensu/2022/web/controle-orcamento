package br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria;

/**
 * Tipos possíveis para uma categoria
 */
public enum TipoCategoria {

    ENTRADA ("Entrada"),
    SAIDA ("Saída");

    private String value;

    TipoCategoria(String tipo) {
        this.value = tipo;
    }
    String getValue() {
        return this.value;
    }
    
}
