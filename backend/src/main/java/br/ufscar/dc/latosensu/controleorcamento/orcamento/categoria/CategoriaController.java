package br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Métodos da API referentes à categoria
 */
@RestController("categoria")
@RequestMapping(
  path = "/categoria",
  produces = MediaType.APPLICATION_JSON_VALUE
)
public class CategoriaController {

  private CategoriaRepository categoriaRepository;

  public CategoriaController(CategoriaRepository categoriaRepository) {
    this.categoriaRepository = categoriaRepository;
  }

  /**
   * Retorna a lista de categorias cadastradas no sistema.
   * @return Lista com todas as categorias cadastradas
   */
  @GetMapping(path = "", consumes= {})
  List<Categoria> listAll() {
    return this.categoriaRepository.findAll();
  }

  /**
   * Adiciona uma categoria válida.
   * @param categoria Categoria que deve ser adicionada
   * @return Categoria adicionada
   */
  @PostMapping(path = "")
  Categoria addCategoria(@RequestBody @Valid Categoria categoria) {
    // throw new CategoriaNaoEncontradaException(1l);
    return this.categoriaRepository.save(categoria);
  }

  /**
   * Remove a categoria que tem o id informado como variável de caminho, ou lança exceção caso a categoria não esteja
   * cadastrada
   * @param categoriaId Id da categoria a ser removida
   */
  @DeleteMapping(path = "/{categoriaId}")
  void removeCategoria(@PathVariable Long categoriaId) {
    Optional<Categoria> categoria =
      this.categoriaRepository.findById(categoriaId);
    categoria.ifPresentOrElse(
      novaCategoria -> {
        this.categoriaRepository.delete(novaCategoria);
      },
      () -> {
        throw new CategoriaNaoEncontradaException(categoriaId);
      }
    );
    return;
  }
}
