package br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento;

import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.Categoria;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

/**
 * Classe que representa as movimentações na conta
 */
@Entity
@Data
public class Lancamento {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NotNull(message = "É necessário informar um valor")
  private BigDecimal valor;

  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  private Categoria categoria;

  @NotNull(message = "É necessário informar uma descrição")
  private String descricao;

  private LocalDate dataBalancete;

  @CreatedDate
  private Date createdDate;

  @UpdateTimestamp
  private Date updatedDate;
}
