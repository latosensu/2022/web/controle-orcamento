package br.ufscar.dc.latosensu.controleorcamento.orcamento.resumo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/resumo" , produces = MediaType.APPLICATION_JSON_VALUE )
public class ResumoController {

    Logger logger = LoggerFactory.getLogger(ResumoController.class);
    private ResumoService resumoService;

    public ResumoController(ResumoService resumoService){
        this.resumoService = resumoService;
    }

    @GetMapping("")
    public Resumo obterResumo() {
        logger.debug("Obtendo Resumo...!");
        return this.resumoService.obterResumo();
    }
    
}
