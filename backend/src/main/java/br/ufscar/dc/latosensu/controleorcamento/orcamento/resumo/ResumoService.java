package br.ufscar.dc.latosensu.controleorcamento.orcamento.resumo;

import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.Categoria;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.Lancamento;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.LancamentoRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class ResumoService {

  private LancamentoRepository<Lancamento, Long> lancamentoRepository;

  public ResumoService(
    LancamentoRepository<Lancamento, Long> lancamentoRepository
  ) {
    this.lancamentoRepository = lancamentoRepository;
  }

  public Resumo obterResumo() {
    LocalDate hoje = LocalDate.now();
    LocalDate primeiroDiaMes = hoje.withDayOfMonth(1);
    
    LocalDate ultimoDiaMes = hoje.withDayOfMonth(hoje.getMonth().length(hoje.isLeapYear()));
    List<Lancamento> lancamentosMes =
      this.lancamentoRepository.findByDataBalanceteBetween(
          primeiroDiaMes,
          ultimoDiaMes
        );
    Map<Categoria, List<Lancamento>> lancamentosPorCategoria = lancamentosMes
      .stream()
      .collect(Collectors.groupingBy(Lancamento::getCategoria));
    return new Resumo(lancamentosPorCategoria);
  }
}
