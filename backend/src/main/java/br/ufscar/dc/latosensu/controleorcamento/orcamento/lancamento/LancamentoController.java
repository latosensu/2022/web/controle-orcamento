package br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
 * APIs referentes a lançamentos
 */
@RestController
@RequestMapping(
  produces = { MediaType.APPLICATION_JSON_VALUE },
  path = { "/lancamento" }
)
@RequiredArgsConstructor
public class LancamentoController {

  @NonNull
  private LancamentoRepositoryImpl lancamentoRepository;

  /**
   * Lista todos os lançamentos cadastrados
   * @return Lançamentos cadastrados
   */
  @GetMapping("")
  List<Lancamento> listarTodos() {
    return lancamentoRepository.findAll();
  }

  /**
   * Busca os lançamentos cadastrados pela descrição
   * @return Lançamentos cadastrados
   */
  @GetMapping("/buscar")
  List<Lancamento> buscarPelaDescricao(
    @PathParam("descricao") String descricao
  ) {
    return lancamentoRepository.findAllByDescricaoILikeUltra(descricao);
  }

  /**
   * Adiciona um novo lançamento
   */
  @PostMapping("")
  Lancamento adicionarLancamento(@RequestBody @Valid Lancamento lancamento) {
    lancamento.setDataBalancete(LocalDate.now());
    return this.lancamentoRepository.save(lancamento);
  }
}
